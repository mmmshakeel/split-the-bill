<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomePageTest extends TestCase
{
    /**
     * Test home page loads
     *
     * @return void
     */
    public function testHomePageLoad()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
