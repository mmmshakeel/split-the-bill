<?php

namespace Tests\Feature;

use App\Helpers\BillHelper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SplitBillTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBillSplit()
    {
        $jsonData = json_decode('{
    "data": [{
            "day": 1,
            "amount": 50,
            "paid_by": "tanu",
            "friends": [
                "kasun",
                "tanu"
            ]
        },
        {
            "day": 2,
            "amount": 100,
            "paid_by": "kasun",
            "friends": [
                "kasun",
                "tanu",
                "shakeel"
            ]
        },
        {
            "day": 3,
            "amount": 100,
            "paid_by": "shakeel",
            "friends": [
                "shakeel",
                "tanu",
                "kasun"
            ]
        },
        {
            "day": 4,
            "amount": 300,
            "paid_by": "shakeel",
            "friends": [
                "shakeel",
                "tanu",
                "kasun"
            ]
        }
    ]
}');
        $result = BillHelper::splitbill($jsonData);

        $this->assertEquals(550, $result['totalSpent']);
        $this->assertEquals(3, $result['totalFriends']);
        $this->assertEquals(133.33, $result['oweToPool']['tanu']);
        $this->assertEquals(83.33, $result['oweToPool']['kasun']);
        $this->assertEquals(-216.67, $result['oweToPool']['shakeel']);
    }
}
