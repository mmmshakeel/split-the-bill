@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>
            @elseif(Session::has('fail'))
                <div class="alert alert-danger" role="alert">
                    {{ Session::get('fail') }}
                </div>
            @endif

                <div id="splitForm" formaction="test-form-action"></div>

            <!-- Modal -->
            <div class="modal fade" id="sampleModal" tabindex="-1" role="dialog" aria-labelledby="sampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="sampleModalLabel">Sample Json Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <pre>
{
    "data": [{
            "day": 1,
            "amount": 50,
            "paid_by": "tanu",
            "friends": [
                "kasun",
                "tanu"
            ]
        },
        {
            "day": 2,
            "amount": 100,
            "paid_by": "kasun",
            "friends": [
                "kasun",
                "tanu",
                "shakeel"
            ]
        },
        {
            "day": 3,
            "amount": 100,
            "paid_by": "shakeel",
            "friends": [
                "shakeel",
                "tanu",
                "kasun"
            ]
        }
    ]
}
                        </pre>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
