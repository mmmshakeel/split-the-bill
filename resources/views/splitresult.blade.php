@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card m-b-3">
                    <div class="card-body">
                        <div class="col-md-6">
                            <div class="col"><h2>Total Bill Value: {{ $totalSpent }}</h2></div>

                            <hr />

                                <h3>Bill Settlement:</h3>

                                <h5>Average for each: {{ $averageSpent }}</h5>
                                @foreach($oweToPool as $key => $value)
                                    @if ($value > 0)
                                        <div class="alert alert-danger" role="alert">
                                            {{ $key }} owes {{ $value }} to the pool
                                        </div>
                                    @else
                                        <div class="alert alert-success" role="alert">
                                            {{ $key }} takes {{ abs($value) }} from the pool
                                        </div>
                                    @endif
                                @endforeach

                            <a class="btn btn-light" href="/">Go Back Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
