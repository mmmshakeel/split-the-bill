import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class SplitForm extends Component {
    constructor(props) {
        super(props);
        this.state = {value: '', isValid : true};

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(val) {
        this.setState({value : val})
    }

    validateJson(jsonString) {
        try {
            let obj = JSON.parse(jsonString);

            return this.isObject(obj);
        } catch (e) {
            return false;
        }
    }

    isObject (value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }

    handleSubmit(event) {
        event.preventDefault();

        let {value} = this.state;
        let form = event.target;


        if (value == "") {
            form.submit();
        } else {

            if (this.validateJson(value)) {
                form.submit();
            } else {
                this.setState({ isValid: false });
            }
        }

    }

    render() {
        let {value, isValid} = this.state;

        return (
            <div className="card m-b-3">
                <div className="card-body">
                    <h2>Add your bills</h2>
                    <hr />
                    <h4>Enter the bills in json format</h4>
                    <button type="button" className="btn btn-link" data-toggle="modal" data-target="#sampleModal">
                        Sample Json Data
                    </button>

                    <form onSubmit = {this.handleSubmit} action = "/splitbill" method = "POST" encType = "multipart/form-data">
                        <div className="form-row m-d-2">
                            <div className="col">
                                <textarea className="form-control fixed-width-font" value={value} onChange={(e) => this.handleChange(e.target.value)} rows="15" name="jsonBillData"></textarea>
                                {
                                    (!isValid) ?
                                        <span className="error text-danger">Invalid Json, please check!</span>
                                        :
                                        null
                                }
                            </div>
                        </div>

                        <hr />
                        <h4>- Or - Upload text file with json data</h4>
                        <div className="form-group">
                            <label htmlFor="jsonFile">Upload Json File</label>
                            <input type="file" name="jsonFile" className="form-control-file" id="jsonFile" />
                        </div>

                        <div className="form-row">
                            <div className="col-md-3">
                                <button type="submit" className="btn btn-primary btn-block">Split It</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

if (document.getElementById('splitForm')) {
    ReactDOM.render(<SplitForm />, document.getElementById('splitForm'));
}
