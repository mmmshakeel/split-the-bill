clone the repo

cd in to the folder `cd split-the-bill`

run `docker-compose up -d`

run the following commands:  
1. docker-compose exec app cp .env.example .env
2. docker-compose exec app composer install
3. docker-compose exec app php artisan key:generate
4. docker-compose exec app npm install
5. docker-compose exec app npm run dev

If you get a permission issue run the following:  
docker-compose exec app chmod -R 777 storage/

Run the tests:  
docker-compose exec app vendor/bin/phpunit