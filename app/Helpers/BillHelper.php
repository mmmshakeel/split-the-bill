<?php

namespace App\Helpers;

class BillHelper {

    /**
     * Split the bill based on json data
     *
     * @param $jsonData
     * @return array
     * @throws \Exception
     */
    public static function splitbill($jsonData)
    {
        $totalSpent = 0;
        $friends = [];
        $friendsPaid = [];

        foreach ($jsonData->data as $data) {
            // get the total spent amount
            $totalSpent += $data->amount;

            // get the friends
            $friends = array_merge($friends, $data->friends);

            // get each friend paid amount
            if (array_key_exists($data->paid_by, $friendsPaid)) {
                $friendsPaid[$data->paid_by] += $data->amount;
            } else {
                $friendsPaid = array_add($friendsPaid, $data->paid_by, $data->amount);
            }
        }

        // get unique friends
        $uniqueFriends = array_unique($friends);

        // get average spend
        $totalFriends = count($uniqueFriends);

        if ($totalFriends > 0) {
            $averageSpent = number_format($totalSpent / $totalFriends, 2);
        } else {
            throw new \Exception('No friends found!');
        }

        // find each owes to the pool
        $oweToPool = [];

        foreach ($friendsPaid as $key => $paid) {
            $oweToPool = array_add($oweToPool, $key, number_format(($averageSpent - $paid), 2));
        }

        return [
            'totalSpent' => $totalSpent,
            'totalFriends' => $totalFriends,
            'averageSpent' => $averageSpent,
            'oweToPool' => $oweToPool
        ];
    }
}