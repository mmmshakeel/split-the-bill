<?php

namespace App\Http\Controllers;

use App\Http\Requests\SplitForm;
use http\Env\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\UploadedFile;
use App\Helpers\BillHelper;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    /**
     * @param SplitForm $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function splitbill(SplitForm $request)
    {

        if (!empty($request->jsonBillData)) {
            // read the json input from text field
            $jsonData = json_decode($request->jsonBillData);

        } else {
            if ($request->hasFile('jsonFile')) {
                // read the json input from file
                $uploadedFile = $request->file('jsonFile');

                // validate uploaded file
                if (!$uploadedFile->isValid()) {
                    Session::flash('fail', 'Invalid uploaded file');
                    return back();
                }

                //validate json content from file
                $jsonData = json_decode(file_get_contents($uploadedFile->path()));

                if (is_null($jsonData)) {
                    Session::flash('fail', 'Invalid Json data in uploaded file');
                    return back();
                }
            } else {
                Session::flash('fail', 'No data given');
                return back();
            }

        }

        try {
            $result = BillHelper::splitbill($jsonData);
        } catch (\Exception $e) {
            Session::flash('fail', $e->getMessage());
            return back();
        }


        // check for total bill value
        if ($result['totalSpent'] == 0) {
            Session::flash('fail', 'Total bill value is 0');
            return back();
        }


        return view('splitresult', [
            'totalSpent' => $result['totalSpent'],
            'totalFriends' => $result['totalFriends'],
            'averageSpent' => $result['averageSpent'],
            'oweToPool' => $result['oweToPool']
        ]);
    }
}
